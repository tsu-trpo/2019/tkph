package ru.tkph.tkph;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class TestActivity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity1);
    }

    public void NextLvlOnClick(View view){
        Intent intent = new Intent(this, TestActivity2.class);
        startActivity(intent);
    }
}
