package ru.tkph.tkph;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class FightActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanseState){
        super.onCreate(savedInstanseState);
        setContentView(R.layout.activity_fight);
        configureBackButton();
    }

private void configureBackButton(){
    Button backButton = (Button) findViewById(R.id.backButton);
    backButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        finish();
        }
    });
}
}